<?php 

namespace App\Middleware;

use Mini\Http\Request;
use DI\Container;

class Auth
{	
	
	public function __invoke(Request $request,Container $c)
	{
		$this->r =  $c->get(\Mini\Router\RouteCollector::class);
		
		if(!isset($_SESSION['user_id']))
			$request->redirect($this->r->urlFor("auth.signIn"));
	}
}