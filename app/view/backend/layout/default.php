<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Backend | <?=$this->e($title)?></title>

    <!-- Bootstrap -->
    <link href="<?=$assets?>vendors/bootstrapV3/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?=$assets?>vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <?=$this->section('style')?>
    <link href="<?=$assets?>css/style.css" rel="stylesheet">

  </head>
  <body>
    <!-- <div class="window"> -->
      <div id="title"><span>Mini <b>CMS |</b> <a href="http://<?=BASE_URL?>" target="_blank"><i class="fa fa-external-link"></i></a></span></div>
      <div id="header"><?=$this->e($title)?></div>
      <div id="user"><span><i class="fa fa-user-circle-o"></i> Dulshan</span></div>

      <div id="side"><?=$this->insert('backend::partial/side')?></div>
      <div id="page"><?=$this->section('content')?></div>
      <!-- <div id="footer">7</div> -->
    <!-- </div> -->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="<?=$assets?>vendors/jquery/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="<?=$assets?>vendors/bootstrapV3/js/bootstrap.min.js"></script>

    <?=$this->section('script')?>

  </body>
</html>
