var ce = document.getElementById("cmeditor");
// Probably needs more kludges to be portable... you get the idea
var text = ce.textContent || ce.innerText;
var editor = CodeMirror(function(node) {
    ce.parentNode.replaceChild(node, ce);
}, {
    value: text,
    lineNumbers: true,
    matchBrackets: true,
    mode: "application/x-httpd-php",
    indentUnit: 4,
    indentWithTabs: true,
    styleActiveLine: true,
});
editor.setOption("theme", "monokai");