<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use Mini\Router\RouteCollector;

$routers->addGroup("/auth", function (RouteCollector $r) {
    $r->get("/signin", ["\App\Controller\AuthController:signInView"])->setName("auth.signIn");
    $r->post("/signin", ["\App\Controller\AuthController:signInHandler"]);
    $r->get("/signup", ["\App\Controller\AuthController:signUpView"])->setName("auth.signUp");
    $r->post("/signup", ["\App\Controller\AuthController:signUpHandler"]);
});

$routers->addGroup('', function (RouteCollector $r) {

    $r->get('/', ["\App\Controller\BackendController:index"])->setName("backend.home");

    $r->get('/page', ["\App\Controller\BackendController:page"])->setName("backend.page");

}, [new \App\Middleware\Auth()]);

// $routers->get('/get/data/[page/id/{opt}]', function ($page, $id, $opt = 123) {
//     echo "This is page $page , id $id , value $opt";
// })->setName("data.one");

// $routers->get('/get/image/[of/ids]', function ($of, $id, $opt = 123) {
//     echo "This is get Data page";
// });

// $routers->get('/get/non/[of/ids]', function () {
//     echo "This is get Data page";
// });

// $routers->get("/load/class/[greet/{name}]", ["\App\Controller\PageController:index"]);

$routers->get("/error", function () {})->setName("app.error");
