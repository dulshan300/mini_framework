<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Controller;

use Mini\MVC\Controller;

/**
 * Description of backendController
 *
 * @author dulshanm
 */
class BackendController extends Controller
{
    //put your code here
    public function index()
    {
        echo $this->render("backend::page/home");
    }

    public function page()
    {
        echo $this->render("backend::page/page");
    }

}
