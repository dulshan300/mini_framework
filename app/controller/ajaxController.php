<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace APP\Controller;

use DI\Container;
use Mini\mvc\Controller;
use Medoo\Medoo;

/**
 * Description of backendController
 *
 * @author dulshanm
 */
class AjaxController extends Controller
{
    //put your code here
    public function index()
    {   
        echo $this->render("backend::page/home");
    }

    public function page()
    {
        echo $this->render("backend::page/page");

    }

    public function cms(Container $c)
    {
        $config    = $c->get('config');
        $theme_dir = "./content/theme/" . $config->get('cms.theme');
        $dir_data  = [];

        if (file_exists($theme_dir)) {
            echo "<pre>";
            $list = $this->scan($theme_dir, $dir_data);
            if (isset($list[0])) {
                $value = $list[0];
                unset($list[0]);
                $list['index'] = $value;
            }
            echo $this->link($list);
            echo "</pre>";
        }
        die();
        echo $this->render("backend::page/cms");

    }

    public function scan($dir, &$list)
    {
        if (is_dir($dir)) {
            $file_list = scandir($dir);
            foreach ($file_list as $key => $file) {
            	if ($file!="." & $file!="..") {
            		if (is_dir("$dir/$file")) {
            			$list[$file]=[];
            			$this->scan("$dir/$file", $list[$file]);
            		}else{
            			$list[]=$file;
            		}
            	}
            }

        } else {
            $list[] = $dir;
        }

        return $list;
    }

    public function link($list)
    {
        $out = "<ul>";

        foreach ($list as $key=>$file) {
            if (is_array($file)) {
                $out .="<li> $key".$this->link($file)."</li>";
                
            }else{
                $out .="<li>$file</li>";
            }

        }

        return $out .= "</ul>";
    }
}
