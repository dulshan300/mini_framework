<?php
declare (strict_types = 1);

session_start();

require_once 'core/defines.php';

require_once 'vendor/autoload.php';

use Mini\App;
use Mini\Router\RouteCollector;
use Symfony\Component\Yaml\Yaml;

//Error Handling
$whoops = new \Whoops\Run;
$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
$whoops->register();

// Loading app settings
$app_config = Yaml::parseFile("./app/config.yaml");

// Setup Database System
$database = new \Medoo\Medoo($app_config['db']);

$app = new App($app_config);

$container = $app->getContainer();

$container->set(\Medoo\Medoo::class, $database);

require_once 'app/service.php';

$routers = new RouteCollector();

require_once 'app/routers.php';

$app->registerRouters($routers);
// run application

$app->run();
