<?php

namespace Mini\Http;

class Request
{

    private $request;
    private $post;
    private $get;

    public function __construct()
    {
        $this->request = $_SERVER;
        $this->post    = $_POST;
        $this->get     = $_GET;
    }

    public function getRequest()
    {
        return $this->request;
    }

    public function getMethod()
    {
        return $this->request['REQUEST_METHOD'];
    }

    public function getRequestedUrl()
    {
        $req_url  = $this->request['REQUEST_URI'];
        $scr_name = $this->request['SCRIPT_NAME'];
        $indexPos = strpos($scr_name, "index.php");
        $url      = substr($req_url, $indexPos);

        return (($url == "") ? "/" : "/" . $url);
    }

    public function getPara($key, $default = false)
    {
        return (isset($this->get[$key])) ? $this->get[$key] : $default;
    }

    public function redirect($path)
    {
        header("Location: $path");
    }

}
