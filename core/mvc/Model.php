<?php
namespace Mini\MVC;

class Model {
	private $db;
	public static $table = null;
	public static $class = null;
	private $fields = [];
	public $editable = [];

	public function __construct() {
		global $database;
		$this->db = $database;
		self::$class = get_called_class();
		if (self::$table === null) {
			$val = explode("\\", self::$class);
			self::$table = strtolower($val[count($val) - 1]);
		}
	}

	public function get($id) {
		$table = self::$table;
		$res = $this->db->select($table, '*', ['id' => $id]);
		if ($res) {
			$o = new self::$class;
			foreach ($res[0] as $key => $value) {
				$o->$key = $value;
			}
			return $o;
		}

		$this->error();

		return false;
	}

	public function all() {
		$out = [];
		$table = self::$table;
		$res = $this->db->select($table, "*");
		if (count($res) > 0) {
			foreach ($res as $row) {
				$o = new self::$class;
				foreach ($row as $key => $value) {
					$o->$key = $value;
				}
				$out[] = $o;
			}

		}

		return $out;
	}

	public function save() {
		if (count($this->editable) > 0) {
			$data = [];
			foreach ($this->fields as $key => $value) {
				if (in_array($key, $this->editable)) {
					$data[$key] = $value;
				}
			}
			$table = self::$table;
			$pdo_st = $this->db->update($table, $data, ['id' => $this->id]);
			if ($pdo_st->rowCount() > 0) {
				return true;
			} else {
				$this->error();
			}
		}

		return false;

	}

	public static function create($data, $if_not = false) {
		global $database;
		$table = self::$table;
		$object = new $table;
		$editable = $object->editable;

		if (count($editable) > 0) {
			$data_allowed = [];
			foreach ($data as $key => $value) {
				if (in_array($key, $editable)) {
					$data_allowed[$key] = $value;
				}
			}

			if ($if_not) {
				$res = $database->select($table, ['id'], $data);
				if (count($res) > 0) {
					$id = $res[0]['id'];
					return $object->get($id);
				}
			}

			$pdo_st = $database->insert($table, $data_allowed);

			if ($pdo_st->rowCount() > 0) {
				return $object->get($database->id());
			} else {
				$inf = $db->error();
				if ($inf[2] !== NULL) {
					$code = $inf[0];
					$msg = $inf[2];
					throw new Exception("Error PDO $code  : $msg ", 1);
				}
			}
		}

		return false;

	}

	function remove() {

		$table = self::$table;

		$pdo_st = $this->db->delete($table, ['id' => $this->id]);

		if (($ef_row = $this->db->exec($sql)) > 0) {
			return $ef_row;
		} else {
			$this->error();
		}
	}

	public function __get($key) {
		$table = self::$table;
		if (key_exists($key, $this->fields)) {
			return $this->fields[$key];
		} else {
			throw new Exception("Error : Property '$key' not found in $table ", 1);
		}
	}

	public function __set($key, $val) {
		$this->fields[$key] = $val;
	}

	private function error() {
		$inf = $this->db->error();
		if ($inf[2] !== NULL) {
			$code = $inf[0];
			$msg = $inf[2];
			throw new Exception("Error PDO $code  : $msg ", 1);
		}
	}
}
