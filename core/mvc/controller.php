<?php
namespace Mini\MVC;

use League\Plates\Engine;

class Controller {
	public $view;

	public function __construct(Engine $view) {
		$this->view = $view;
	}

	public function render($path, $data = []) {
		return $this->view->render($path, $data);
	}

}
