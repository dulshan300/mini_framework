<?php 

namespace Mini\MVC;


use Mini\Http\Request;
use DI\Container;

abstract class Middleware
{

	abstract function __invoke(Request $request,Container $c);
	
}