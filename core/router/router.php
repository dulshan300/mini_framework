<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mini\Router;

/**
 * Description of router
 * @example /url/for/[data/of/{optional/data}]
 * @example /url/for/[{optional/data}]
 * @example /url/for
 * @author dulshanm
 */
class Router
{

    private $name, $routerData, $handler, $httpMethod, $middleware;

    /**
     * Router constructor.
     * @param $httpMethod
     * @param $routerData
     * @param $handler
     * @param string $name
     */
    public function __construct($httpMethod, $routerData, $handler, array $middleware = [], $name = "")
    {
        $this->httpMethod = $httpMethod;
        $this->routerData = $routerData;
        $this->handler    = $handler;
        $this->name       = $name;
        $this->middleware = $middleware;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getRouterData()
    {
        return $this->routerData;
    }

    /**
     * @return callable | string
     */
    public function getHandler()
    {
        return $this->handler;
    }

    /**
     * @return string
     */
    public function getHttpMethod()
    {
        return $this->httpMethod;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param $routerData
     * @return $this
     */
    public function setRouterData($routerData)
    {
        $this->routerData = $routerData;
        return $this;
    }

    /**
     * @param $handler
     * @return $this
     */
    public function setHandler($handler)
    {
        $this->handler = $handler;
        return $this;
    }

    /**
     * @param $httpMethod
     * @return $this
     */
    public function setHttpMethod($httpMethod)
    {
        $this->httpMethod = $httpMethod;
        return $this;
    }

    /**
     * @return array
     */
    public function getRouterDataArray()
    {
        /**
         * [
         *      0=> url
         *      1=> req parameters
         *      2=> opt parametes
         * ]
         */
        preg_match_all('/([\/\w\d_-]+)/', $this->routerData, $output_array);

        return $output_array[0];
    }

    public function hasParameters(): bool
    {
        if (count($this->getRouterDataArray()) > 1) {
            return true;
        }

        return false;
    }

    public static function instant(Router $router): Router
    {
        return $router;
    }

    public function getMiddlware()
    {
        return $this->middleware;
    }

}
