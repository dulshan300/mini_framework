<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mini\Router;

use DI\Container;
use Mini\Http\Request;
use Mini\Router\Implement\Dispatcher;

/**
 * Description of dispatcher
 *
 * @author dulshanm
 */
class RouterDispatcher implements Dispatcher
{
    private $routers;
    private $container;

    //  Router Data Extraction
    public function __construct($routers, Container $container)
    {
        $this->routers   = $routers;
        $this->container = $container;
    }

    public function dispatch($httpMethod, $uri)
    {
        $output = ['error' => Dispatcher::NOT_FOUND, 'router' => null];

        foreach ($this->routers[$httpMethod] as $router) {
            $routerData = trim($router->getRouterDataArray()[0]);
            $routerData = (strrpos($routerData, '/') == strlen($routerData) - 1) ? $routerData : $routerData . '/';

            $uri_uri = (strrpos($uri, "/") == strlen($uri) - 1) ? $uri : $uri . "/";

            if (!$router->hasParameters()) {
                if ($routerData === $uri_uri) {
                    $output = ['error' => Dispatcher::FOUND, 'router' => $router];
                    break;
                }
            } elseif ((stripos($uri_uri, $routerData)) !== false) {
                $output = ['error' => Dispatcher::FOUND, 'router' => $router];
                break;
            }

        }

        return $output;
    }

    public function prase(Router $r, $uri)
    {
//        1)    There are routers which has paramers and no parametes
        //        2)    There are callables which has paramers and user not provided proper args
        $paraMap = [];

        if ($r->hasParameters()) {
            $routerData = $r->getRouterDataArray();
            // url
            $router_url = preg_replace("/\/$/", "", $routerData[0]);

            $router_req_para = preg_replace("/\/$/", "", $routerData[1]);
            $router_req_para = explode("/", $router_req_para);
            $router_opt_para = [];

            $url_data = preg_replace("/\/$/", "", $uri);

            if (isset($routerData[2])) {
                $router_opt_para = preg_replace("/\/$/", "", $routerData[2]);
                $router_opt_para = explode("/", $router_opt_para);
            }

            if (count($router_req_para) > 0 && (strlen($router_url) >= strlen($url_data))) {
                throw new \Exception("Insufficient arguments given, " . count($router_req_para) . " required 0 given");
            }

            $url_data = preg_replace("/^\//", "", str_replace($router_url, "", $url_data));

            $url_data = explode("/", $url_data);

            if (count($router_req_para) > count($url_data)) {
                throw new \Exception("Insufficient arguments given, " . count($router_req_para) . " required " . count($url_data) . " given");
            }

            foreach (array_merge($router_req_para, $router_opt_para) as $key => $val) {
                if (isset($url_data[$key])) {
                    $paraMap[$val] = $url_data[$key];
                } else {
                    break;
                }
            }
        }

        $handler = $r->getHandler();

        if (is_callable($handler)) {
            $calback = new \ReflectionFunction($handler);
            $this->func_execute($calback, $paraMap);

        } elseif (is_array($handler)) {
            $data = explode(":", $handler[0]);
            if (count($data) == 1) {
                throw new \Exception($data[0] . " execution method not provided", 1);
            }
            list($class, $method) = $data;

            $controller = new $class($this->container->get(\League\Plates\Engine::class));

            $ro = new \ReflectionObject($controller);

            if ($ro->hasMethod($method)) {
                $calback = $ro->getMethod($method);
                $this->func_execute($calback, $paraMap, $controller);
            } else {

                throw new \Exception("Method $method not found in $class", 1);
            }
        } else {
            throw new \Exception("Unknown Clouser Type", 1);
        }

//        var_dump($url_data, array_merge($router_req_para,$router_opt_para), $uri);

    }

    public function func_execute($calback, $paraMap, $object = null)
    {

        $fparas = $calback->getParameters();

        if (count($fparas) == 0) {
            if (is_null($object)) {
                $calback->invoke();
            } else {
                $calback->invoke($object);
            }
        } else {
//                map parameters according to the calback

            $paraMap2 = [];
            foreach ($fparas as $i => $value) {
                if ($value->hasType()) {
                    $paraMap2[] = $this->container->get($value->getClass()->getName());
                }
                if (isset($paraMap[$value->name])) {
                    $paraMap2[] = $paraMap[$value->name];
                }
            }
            if (is_null($object)) {
                $calback->invokeArgs($paraMap2);
            } else {
                $calback->invokeArgs($object, $paraMap2);
            }
        }
    }

    public function handleMiddleware($middlewareCollection)
    {
        foreach ($middlewareCollection as $object) {
            $object($this->container->get(Request::class), $this->container);
        }
    }

}
