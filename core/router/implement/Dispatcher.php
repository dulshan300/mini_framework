<?php
/**
 * Created by PhpStorm.
 * User: DulshanM
 * Date: 1/23/2020
 * Time: 8:48 PM
 */

namespace Mini\Router\Implement;


interface Dispatcher
{
    const NOT_FOUND = 0, FOUND = 1, METHOD_NOT_ALLOWED = 2;

    public function dispatch($httpMethod, $uri);
}