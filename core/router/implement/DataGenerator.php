<?php
/**
 * Created by PhpStorm.
 * User: DulshanM
 * Date: 1/23/2020
 * Time: 8:49 PM
 */

namespace Mini\Router\Implement;


interface DataGenerator
{
    public function addRoute($httpMethod, $routeData, $handler);

    public function getData();

}