<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Mini\Router;

use Mini\Router\Implement\DataGenerator;

/**
 * Description of router
 *
 * @author dulshanm
 */
class RouteCollector implements DataGenerator
{

    private $routers;
    private $rouer_prefix;
    private $middleware = [];

    // set Get Request

    public function get($routeData, $handler): Router
    {
        return $this->addRoute('GET', $routeData, $handler, $this->middleware);
    }

    // set Post Request
    public function post($routeData, $handler): Router
    {
        return $this->addRoute('POST', $routeData, $handler, $this->middleware);
    }

    public function addRoute($httpMethod, $routeData, $handler, array $middleware = [], $name = null): Router
    {
        $route                                    = new Router($httpMethod, $this->rouer_prefix . $routeData, $handler, $middleware, (is_null($name)) ? uniqid("R") : $name);
        $this->routers[strtoupper($httpMethod)][] = &$route;
        return $route;
    }

    public function addGroup($groupName, callable $routerCollector, $middleware = [])
    {
        $this->rouer_prefix = $groupName;
        $this->middleware   = $middleware;
        $routerCollector($this);
        $this->middleware   = [];
        $this->rouer_prefix = "";
        return $this;
    }

    public function urlFor($routerName, array $paraList = []): string
    {
        if (isset($this->routers['POST'])) {
            $all_routers = array_merge($this->routers['GET'], $this->routers['POST']);
        } else {
            $all_routers = $this->routers['GET'];
        }

        foreach ($all_routers as $router) {

            if ($router->getName() == $routerName) {
                $routerData = $router->getRouterDataArray();
                if ($router->hasParameters()) {

                    $router_req_para = preg_replace("/\/$/", "", $routerData[1]);
                    $router_req_para = explode("/", $router_req_para);
                    $router_opt_para = [];

//                    Will Hold the output req and opt parametes to implode
                    $out_req = [];
                    $out_opt = [];

                    if (isset($routerData[2])) {
                        $router_opt_para = preg_replace("/\/$/", "", $routerData[2]);
                        $router_opt_para = explode("/", $router_opt_para);

                        foreach ($router_opt_para as $key) {
                            if (key_exists($key, $paraList)) {
                                $out_opt[] = $paraList[$key];
                            }
                        }
                    }
                    foreach ($router_req_para as $key) {
                        if (key_exists($key, $paraList)) {
                            $out_req[] = $paraList[$key];
                        } else {
                            throw new \Exception("Required Argument : $key not given", 1);
                        }
                    }
                    $out = array_merge($out_req, $out_opt);
                    return $_SERVER['REQUEST_SCHEME'] . "://" . BASE_URL . $routerData[0] . implode("/", $out);

                } else {
                    return $_SERVER['REQUEST_SCHEME'] . "://" . BASE_URL . $routerData[0];
                }
                break;
            }
        }

        throw new \Exception("Undefine Router Name: " . $routerName, 1);

    }

    /**
     * @return array(Router)
     */
    public function getData()
    {
        return $this->routers;
    }

    public function dump()
    {
        echo '<pre>';
        var_dump($this->routers);
    }

}
