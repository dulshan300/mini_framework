<?php

namespace Mini;

use League\Plates\Engine;
use Mini\Http\Request;
use Mini\router\implement\Dispatcher;
use Mini\Router\RouteCollector;
use Mini\Router\RouterDispatcher;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

//use Mini\router\Router;

class App
{

    private $config;
    private $request;
    private $routers;
    private $container;

    public function __construct($config = null)
    {
//        Load Configuration
        $this->config    = new \Gestalt\Configuration($config);
        $this->container = \DI\ContainerBuilder::buildDevContainer();
        $this->request   = new Request();
        $this->routers   = new RouteCollector();

//        Setup Logging System
        $logger = new Logger("MINI");
        $logger->pushHandler(new StreamHandler('./logs/info.log', Logger::INFO));

//          Setup Request Class
        $this->container->set(Request::class, $this->request);

//        Setup Template engine ----------------------------------------------------------------------------
        $template = new Engine();
        $template->addFolder("backend", "./app/view/backend");

        $template->addData([
            'assets' => "http://" . BASE_URL . "/app/view/assets/",
            'config' => $this->config,
        ]);

        $template->registerFunction("urlFor", function ($name, $para = []) {
            return $this->container->get(RouteCollector::class)->urlFor($name, $para);
        });
//       --------------------------------------------------------------------------------------------------
        $this->container->set(Engine::class, $template);

        $this->container->set(Logger::class, $logger);
        $this->container->set('config', $this->config);
    }

    public function registerRouters(RouteCollector $r)
    {
        $this->routers = $r;
        $this->container->set(RouteCollector::class, $this->routers);
    }

    public function run()
    {
        $uri = $this->request->getRequestedUrl();

        $dispatch   = new RouterDispatcher($this->routers->getData(), $this->container);
        $httpMethod = $this->request->getMethod();

        $dispatchData = $dispatch->dispatch($httpMethod, $uri);

        switch ($dispatchData['error']) {
            case Dispatcher::NOT_FOUND:
                throw new \Exception("URL '$uri' Not found", 1);
                break;
            case Dispatcher::METHOD_NOT_ALLOWED;
                echo "METHOD_NOT_ALLOWED";
                break;
            case Dispatcher::FOUND;
                $router = $dispatchData['router'];
                $dispatch->handleMiddleware($router->getMiddlware());
                $dispatch->prase($router, $uri);
                break;
        }

    }

    public function getContainer(): \DI\Container
    {
        return $this->container;
    }

}
